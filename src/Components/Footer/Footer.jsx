import React from 'react'
import {Box} from "../common/StylesElements/Grid/Grid";
import {WrapperList} from "../common/StylesElements/Wrappers/TextWrapper";
import ItemList from "../common/StylesElements/Blocks/ItemElements";
import facebookIcon from "../../assets/facebook.svg"
import twitterIcon from "../../assets/twitter.svg"
import youtubeIcon from "../../assets/youtube.svg"
import redditIcon from "../../assets/reddit.svg"
import {Image} from "../common/StylesElements/Images/Image";

const Footer = () => {

    const style_box = `
        flex: 0 0 auto;
        width: 100%;
    `

    const style_wrap = `
        width: 100%;
        padding: 25px 0;
        text-align: center;
         display: inline-block;
         margin-top: 77px;
        & li{
            width: 33px;
            height: 33px;
            display: inline-block;
            margin: 0 6px;
        }
    `


    return(
        <Box style_c={style_box}>
            <WrapperList style_c={style_wrap}>
                <ItemList> <a href="https://www.facebook.com/" target="_blank"><Image src={facebookIcon} name={'facebook'} /></a> </ItemList>
                <ItemList> <a href="https://twitter.com/" target="_blank"><Image src={twitterIcon} name={'twitter'} /></a> </ItemList>
                <ItemList> <a href="https://www.youtube.com/" target="_blank"><Image src={youtubeIcon} name={'youtube'} /></a> </ItemList>
                <ItemList> <a href="https://www.reddit.com/" target="_blank"><Image src={redditIcon} name={'reddit'} /></a> </ItemList>
            </WrapperList>
        </Box>
    )
}

export default Footer