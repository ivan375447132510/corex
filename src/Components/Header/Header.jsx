import React from "react";
import {Box, Col} from "../common/StylesElements/Grid/Grid";
import logoIcon from "../../assets/mainLogo.png";
import logoMobailIcon from "../../assets/logoMobail.png";
import lengIcon from "../../assets/Group.png";
import cartIcon from "../../assets/Group 117.svg"
import burgerMenuIcon from "../../assets/burgerMenu.svg"
import {Image} from "../common/StylesElements/Images/Image";
import WrapperText from "../common/StylesElements/Wrappers/TextWrapper";
import { notification } from 'antd';
import {compose} from "redux";
import {connect} from "react-redux";

const Header = ({card, ...props}) => {

    const style_box = `
        padding:26px 28px; 
        justify-content: space-between;
        flex: 0 0 auto;
        width:100%;
        @media (max-width: 767px){
            padding-top: 61px;
        }
        @media (max-width: 424px){
            padding-top: 29px;
            padding-bottom: 29px;
        }
        
    `

    const style_col =  `
        display: flex;
        align-items: center;
        font-size: 0px;
    `

    const style_cart = `
        display: flex;
        height: 100%;
        align-items: center;
    `

    const onWarning = () => {
        notification['warning']({
            message: 'Функция в разработке',
        })
    }

    const style_count_card = `
            position: relative;
            & .count_card{
                position: absolute;
                right: -8px;
                top: -2px;
                width: 17px;
                height: 17px;
                background: #0B0C23;
                display: flex;
                align-items: center;
                justify-content: center;
                z-index: 10;
                border-radius: 50%;
            }
            @media (max-width: 767px){
            & div img{
                width: 31px;
            }
        }
    `

    return(
        <Box style_c={style_box}>
            {
                props.sizeWindows !== 'md' && props.sizeWindows !== 'sm' &&
                <Col style_c={style_col}>
                    <Image src={logoIcon} name={'logoName'} />
                    <div onClick={ onWarning } ><Image  src={lengIcon} alt="leng" style_c={'margin: 0 15px 0 28px;cursor: pointer;'} /></div>
                    <div onClick={ onWarning } ><WrapperText style_c={'text-transform: uppercase;cursor: pointer;'} > eng </WrapperText></div>
                </Col>
            }

            {
                props.sizeWindows !== 'xl' && props.sizeWindows !== 'lg' &&
                <>
                    <Col>
                        <Image style_c={'width:31px;'} src={burgerMenuIcon} name={'burger'} />
                    </Col>
                    <Col>
                        <Image style_c={'width:105px;'} src={logoMobailIcon} name={'logoName'} />
                    </Col>
                </>
            }

            <Col style_c={style_count_card}>
                    <span className="count_card">{card ? card.length : 0}</span>
                    <Image src={cartIcon} name="cart" style_c={style_cart} />
            </Col>
        </Box>
    )
}

const mapStateToProps = (state) => {
    return{
        card: state.app.card,
        sizeWindows: state.app.sizeWindows,

    }
}

export default compose(
    connect( mapStateToProps, {} )
)(Header)
