import React, {useState, useEffect} from 'react'
import {Box, Col} from "../common/StylesElements/Grid/Grid";
import SortElement from "../common/Parts/Sotr/SotrElement";
import ProductsElement from "../common/Parts/Products/ProductsElement";
import SliderNewsElement from "../common/Parts/SliderNews/SliderNewsElement";
import {compose} from "redux";
import {connect} from "react-redux";
import {getProducts, cardAdd} from "../../Redux/app-reducer";
import { message} from 'antd';

const Home = (props) => {

    const [products, setProducts] = useState(null)

    useEffect(() => {
        props.product && props.product.length === 0 && props.getProducts()
        setProducts(props.product)
    }, [props.product]);

    useEffect(() => {
        props.productInProgress.length > 0 && message.loading('Добавление в корзину...', 1 )
    }, [props.productInProgress]);

    useEffect(() => {
        props.lastItem && message.success(`Товар ${props.lastItem.model} успешно добавлен`);
    }, [props.lastItem]);

    const style_box = `
        padding-top: 79px;
        justify-content: center;
        flex: 1 0 auto;
        width: 100%;
        @media (max-width: 767px){
            padding-top: 33px;
        }
        @media (max-width: 424px){
            padding-left: 15px;
            padding-right: 15px;
            padding-top: 18px;
        }
    `

    const style_box_sort = `
        width: 24%;
        @media (max-width: 1439px){
            width: 34%;
        }
        @media (max-width: 1023px){
            width: 30%;
        }
    `
    const style_box_products = `
        width: 52%;
        @media (max-width: 1439px){
            width: 66%;
        }
        @media (max-width: 1023px){
            width: 70%;
        }
        @media (max-width: 767px){
            width: 100%;
        }
        
    `

    return(
        <Box style_c={style_box}>
            {
                props.sizeWindows !== 'md' && props.sizeWindows !== 'sm' &&
                 <Col style_c={style_box_sort}>
                    <SortElement/>
                </Col>
            }

            <Col style_c={style_box_products} >
                <ProductsElement
                    products={products}
                    cardAdd={props.cardAdd}
                    card={props.card}
                    productInProgress={props.productInProgress}
                    lastItem={props.lastItem}
                    sizeWindows={props.sizeWindows}
                />
            </Col>
            {
                props.sizeWindows === 'xl'
                && <Col style_c={'width: 24%;'}>
                    <SliderNewsElement />
                </Col>
            }

        </Box>
    )
}

const mapStateToProps = (state) => {
    return{
        product: state.app.product,
        card: state.app.card,
        productInProgress: state.app.productInProgress,
        lastItem: state.app.lastItem,
        sizeWindows: state.app.sizeWindows
    }
}

export default compose(
    connect( mapStateToProps, {getProducts, cardAdd} )
)(Home)