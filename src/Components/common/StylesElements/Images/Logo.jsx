import React from 'react';
import styled from "styled-components";


export const Logo = ({ style_c, src, name = null, ...props}) => {

    return(
        <Box style_c={style_c}>
            <img src={src} alt={ name ? name : 'logo' } />
        </Box>
    )
}

const Box = styled.div`
        display: inline-block;
        & img{width: 100%;}
        ${props => props.style_c }
    `


