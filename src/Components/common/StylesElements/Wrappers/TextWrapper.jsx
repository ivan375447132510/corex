import React from 'react';
import styled from "styled-components";


const WrapperText = ({style_c, ...props}) => {
    return(
        <Wrapper style_c={style_c}>{props.children}</Wrapper>
    )
}

const Wrapper = styled.div`
    position: relative;
    font-size: 14px;
    line-height: 16px;
    ${props => props.style_c}
`

export default WrapperText



export const WrapperList = ({style_c, ...props}) => {
    return(
        <WrapperUl style_c={style_c}>{props.children}</WrapperUl>
    )
}

const WrapperUl = styled.ul`
    position: relative;
    font-size: 14px;
    line-height: 16px;
    padding: 0;
    margin-bottom: 0;
    ${props => props.style_c}
`


// TABLE
export const WrapperTable = ({style_c, ...props}) => {
    return(
        <WrapperTbl style_c={style_c}>{props.children}</WrapperTbl>
    )
}

const WrapperTbl = styled.table`
    // table-layout: fixed;
    table-layout: inherit;
    width: 100%;
    border-collapse: initial;
    position: relative;
    font-size: 14px;
    line-height: 16px;
    padding: 0;
    width: 100%;
    border-spacing: 0;
    border-spacing: 0 14px;
    ${props => props.style_c}
`



export const WrapperThead = ({style_c, ...props}) => {
    return(
        <WrapperHeadTable style_c={style_c}>{props.children}</WrapperHeadTable>
    )
}

const WrapperHeadTable = styled.thead`
    ${props => props.style_c}
`


export const WrapperTBody = ({style_c, ...props}) => {
    return(
        <WrapperBodyTable style_c={style_c}>{props.children}</WrapperBodyTable>
    )
}

const WrapperBodyTable = styled.tbody`
    & tr td{
    padding:5px;
    color: #C4C4C4;
    transition: .5s;
    cursor: pointer;
    }
    & tr:hover td{
    color:#fff;
    transition: .5s;
    }
    ${props => props.style_c}
`