import React from 'react';
import styled from "styled-components";


const MainWrappers = ({style_c, ...props}) => {
    return(
        <Wrapper style_c={style_c}>{props.children}</Wrapper>
    )
}

const Wrapper = styled.div`
    position: relative;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    ${props => props.style_c}
`

export default MainWrappers