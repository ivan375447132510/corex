import React from 'react';
import styled from "styled-components";


export const Box = ({ style_c, ...props}) => {

    return(
        <Wrapper style_c={style_c}>{props.children}</Wrapper>
    )
}

const Wrapper = styled.div`
        max-width: 1440px;
        margin: 0 auto;
        display: flex;
        flex-wrap: wrap;
        padding-left: 28px;
        padding-right: 28px;
        color:#fff;
        @media (max-width: 1439px){
            max-width: 100%;
        }
        @media (max-width: 767px){
            padding-left: 31px;
            padding-right: 31px;
        }
        ${props => props.style_c }
    `


export const Col = ({style_c, ...props}) => {

    return(
        <Colum style_c={style_c}>{props.children}</Colum>
    )
}

const Colum = styled.div`
        ${props => props.style_c}
    `;

