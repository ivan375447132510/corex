import React from 'react';
import styled from "styled-components";


export const MainText = ({ style_c, ...props}) => {

    return(
        <Wrapper style_c={style_c}>{props.children}</Wrapper>
    )
}

const Wrapper = styled.div`
        font-size: 24px;
        line-height: 48px;
        display: flex;
        align-items: center;
        text-transform: uppercase;
        color:#fff;
        ${props => props.style_c }
    `

