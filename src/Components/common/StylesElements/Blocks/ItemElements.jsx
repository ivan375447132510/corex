import React from 'react';
import styled from "styled-components";


const ItemList = ({ style_c, onclick = null,  isCard = false, ...props}) => {

    return(
        <Wrapper onClick={onclick} isCard={isCard} style_c={style_c}>
            <Point style_c={'left: 0;top: 0;'} />
            <Point style_c={'right: 0;top: 0;'}/>
            <Point style_c={'left: 0;bottom: 0;'}/>
            <Point style_c={'right: 0;bottom: 0;'}/>
            {props.children}
        </Wrapper>
    )
}

export const Point = ({style_c}) => {
    return(
        <Style_point style_c={style_c}/>
    )
}

const Style_point = styled.div`
    position: absolute;
    width: 2px;
    height: 2px;
    background: #fff;
    ${props => props.style_c &&  props.style_c }
`

const Wrapper = styled.li`
    position: relative;
    font-size: 12px;
    line-height: 30px;
    display: flex;
    align-items: center;
    background-color: rgba(255,255,255,${ props => props.isCard ? '.2' : '.1' });
    margin: 14px 0;
    padding: 0 9px;
    width: 100%;
        ${props => props.style_c }
    `

export default ItemList



