import React from 'react';
import styled from "styled-components";

export const TrElement = ({style_c, onclick = null, isCard = false, ...props}) => {

    const onFn = () => {
        onclick && onclick()
    }

    return(
        <>
        <WrapperTr onClick={onclick} isCard={isCard} style_c={style_c}>
            {props.children}
        </WrapperTr>
        </>
    )
}

const WrapperTr = styled.tr`
    position: relative;
    transition: .5s;
    background-color: rgba(255,255,255,${ props => props.isCard ? '.2' : '.1' });
    & td:last-child{padding-right: 9px;}
    td:first-of-type:before{
    content: '';
    position: absolute;
    width: 2px;
    height: 2px;
    background: #fff;
    top:0;
    left:0;
    }
    
    td:first-of-type:after{
    content: '';
    position: absolute;
    width: 2px;
    height: 2px;
    background: #fff;
    bottom:0;
    left:0;
    }
    
    td:last-of-type:before{
    content: '';
     position: absolute;
    width: 2px;
    height: 2px;
    background: #fff;
    right: 0;
    top: 0;
    }
    td:last-of-type:after{
    content: '';
     position: absolute;
    width: 2px;
    height: 2px;
    background: #fff;
    right:0;
    bottom:0;
    }
    & td:first-child{
    padding-left: 9px;
    padding-right: 0;
    min-width: 35px;
    }
    
    ${props => props.style_c}
`

export const TdElement = ({style_c, ...props}) => {
    return(
        <WrapperTd style_c={style_c}>{props.children}</WrapperTd>
    )
}

const WrapperTd = styled.td`
    position: relative;
    padding: 0 5px;
    font-size: 12px;
    line-height: 30px;
    text-align: center;
    ${props => props.style_c}
`