import React from 'react';
import {MainTitle} from "../../StylesElements/Titles/MainTitle";
import WrapperText from "../../StylesElements/Wrappers/TextWrapper";
import 'antd/dist/antd.css';
import {Carousel} from "antd";
import SlideItemElement from "./SlideItemElement";
import allBanner from "../../../../assets/allBanner.png"
import styled from "styled-components";

const SliderNewsElement = () => {

    const style_box = `
        width:209px;
        margin-left: 64px;
    `


    return(
        <WrapperText style_c={style_box}>
            <MainTitle> NEWS </MainTitle>
            <WrapperText style_c={'margin-top:14px;'}>
                <BoxCarousel>
                    <Carousel
                        swipe={true}
                        swipeToSlide={true}
                        autoplay={true}
                        speed={2000}
                        autoplaySpeed={7000}
                        draggable={true}
                        useTransform={true}
                        vertical={true}
                    >
                        <SlideItemElement src={allBanner} title={'banner'} href="/" />
                        <SlideItemElement src={allBanner} title={'banner'} />
                        <SlideItemElement src={allBanner} title={'banner'} />

                    </Carousel>
                </BoxCarousel>
            </WrapperText>
        </WrapperText>
    )
}

const BoxCarousel = styled.div`
    & .ant-carousel .slick-dots{
        flex-direction: column;
        right: 0;
        left: auto; 
    }
    & .ant-carousel .slick-dots li{
    position: relative;
    width: 13px;
    height: 13px;
    background: rgba(0, 0, 0, 0.0001);
    border: 2px solid #FFFFFF;
    box-sizing: border-box;
    margin-top: 7px;
    }
    & .ant-carousel .slick-dots li.slick-active button{
        display: block;
        position: absolute;
        width: 5px;
        height: 5px;
        top: 2px;
        left: 2px;
    }
    & .ant-carousel .slick-dots li button{
            display: block;
            position: absolute;
            width: 5px;
            height: 5px;
            top: 2px;
            left: 2px;
        }
`

export default SliderNewsElement