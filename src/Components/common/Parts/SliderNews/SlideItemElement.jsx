import React from 'react';
import {NavLink} from "react-router-dom";
import WrapperText from "../../StylesElements/Wrappers/TextWrapper";
import {Image} from "../../StylesElements/Images/Image";

const SlideItemElement = ({src, title = null, href = null}) => {


    return(
        <WrapperText>
            {
                href
                    ? <NavLink to={href}><Image src={src} name={ title && title } /> </NavLink>
                    : <Image src={src} name={ title && title } />
            }
        </WrapperText>
    )
}

export default SlideItemElement