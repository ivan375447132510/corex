import React from 'react';
import styled from "styled-components";
import { LoadingOutlined } from '@ant-design/icons';

const PreloaderDefault = () => {
    return(
        <Box>
            <div className="box_preloader">
                <LoadingOutlined /> <br/> Загрузка...
            </div>
        </Box>
    )
}

const Box = styled.div`
    position: fixed;
    width: 100%;
    height: 100vh;
    top: 0;
    left: 0;
    background: #0B0C23;
    & .box_preloader{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
        font-size: 24px;
        color: #fff;
    }
`

export default PreloaderDefault