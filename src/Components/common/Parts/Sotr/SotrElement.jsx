import React from 'react';
import {MainTitle} from "../../StylesElements/Titles/MainTitle";
import WrapperText, {WrapperList} from "../../StylesElements/Wrappers/TextWrapper";
import ItemList from "../../StylesElements/Blocks/ItemElements";
import {notification } from 'antd';

const SortElement = () => {

    const style_box = `
        width:215px;
        margin-left: 60px;
        & li{
        cursor: pointer;
        & div.warning{width: 100%;}
        }
        @media (max-width: 1023px){
            margin-left: 0;
        }
        @media (max-width: 815px){
            width: 175px;
        }
            
    `

    const onWarning = () => {
        notification['warning']({
            message: 'Функция в разработке',
        })
    }

    return(
        <WrapperText style_c={style_box}>
            <MainTitle> SORT BY </MainTitle>
            <WrapperList>
                <ItemList> <div className="warning" onClick={onWarning}> By Manufacturer </div>  </ItemList>
                <ItemList> <div className="warning" onClick={onWarning}> Minimum price </div> </ItemList>
                <ItemList> <div className="warning" onClick={onWarning}> Maximum price </div> </ItemList>
            </WrapperList>
        </WrapperText>
    )
}

export default SortElement