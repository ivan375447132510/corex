import React from 'react';
import WrapperText, {
    WrapperList,
    WrapperTable,
    WrapperTBody,
    WrapperThead
} from "../../StylesElements/Wrappers/TextWrapper";
import style from "../../StylesElements/Style.module.css"
import {MainTitle} from "../../StylesElements/Titles/MainTitle";
import {TdElement, TrElement} from "../../StylesElements/Blocks/TableElements";
import starIcon from "../../../../assets/star.svg"
import greenArrowIcon from "../../../../assets/greenArrow.svg"
import {Image} from "../../StylesElements/Images/Image";
import 'antd/dist/antd.css';
import { message} from 'antd';
import ItemList from "../../StylesElements/Blocks/ItemElements";
import {Col} from "../../StylesElements/Grid/Grid";
import {MainText} from "../../StylesElements/Titles/MainText";

const ProductsElement = ({products, cardAdd, card, productInProgress, sizeWindows, ...props}) => {



    const style_box = `
        min-width: 715px;
        @media (max-width: 1439px){
            min-width: auto;
            width: 89.6%;
        }
        @media (max-width: 1023px){
            width: 100%;
        }
    `

    const wrapper_list_Style = `
        & li{
        padding: 58px;
        flex-wrap: wrap;
        margin: 15px 0;
        }
        @media (max-width: 424px){
            & li{
            padding: 21px 29px;
            }
            & li:first-child{
            margin-top: 0px;
            }
            & li:last-child{
            margin-bottom: 0px;
            }
        }
    `

    const style_text = `
        text-transform: none;
        font-size: 24px;
        line-height: 30px;
        display: flex;
        align-items: center;
        color: #C4C4C4;
        @media (max-width: 424px){
            font-size: 16px;
            line-height: 18px;
        } 
    `

    const style_img = `
        height: 333px;
        width: 100%;
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
        margin: 73px 0 77px;
        & img{
            width: auto;
        }
        @media (max-width: 550px){
            margin: 50px 0;
            & img{
            width: 100%;
        }
        @media (max-width: 424px){
            height: 180px;
            margin: 44px 0;
            & img{
            width: auto;
        }
        }
    `

    const style_price = `
        font-style: normal;
        font-weight: bold;
        font-size: 46px;
        line-height: 100%;
        @media (max-width: 550px){
            font-size: 28px;
        }
        @media (max-width: 424px){
            font-size: 24px;
        }
    `

    const style_block_top = `
        display: flex;
         width: 100%;
         justify-content: space-between;
         & img{
            width: 40px;
            @media (max-width: 550px){
               width: 20px; 
            }
         }
    `



    const onFn = (elem) => {
        if(productInProgress.some( p => p === elem.id)){
            message.warning('Подождите товар добавляется')
        }else{
            card.some( p => p.id === elem.id)
                ? message.warning('Товар уже добавлен в корзину')
                : cardAdd(elem)
        }

    }

    return(
        <>
        {
            sizeWindows !== 'md' && sizeWindows !== 'sm' &&

                <WrapperText style_c={style_box}>
                    <MainTitle> ON SALE </MainTitle>

                    <WrapperTable>

                        <WrapperThead>
                            <TrElement>
                                <TdElement>  </TdElement>
                                { sizeWindows === 'xl' && <TdElement> Release  </TdElement> }
                                <TdElement> Manufacturer  </TdElement>
                                <TdElement> Model </TdElement>
                                <TdElement> Hash </TdElement>
                                { sizeWindows === 'xl' && <TdElement> Algorithm </TdElement> }
                                <TdElement> Efficiency </TdElement>
                                <TdElement style_c={'text-align: left;'}> Profit </TdElement>
                                <TdElement> Price </TdElement>
                            </TrElement>
                        </WrapperThead>

                        <WrapperTBody>

                            {
                                products &&  products.map( elem => {
                                    return(

                                        <TrElement isCard={card.some( p => p.id === elem.id) && true } onclick={ () => onFn(elem) } key={elem.id}>
                                            <TdElement style_c={'display: flex;justify-content: space-between;'}>
                                                <Image src={starIcon} alt="star" /> <Image src={greenArrowIcon} alt="arrow" />
                                            </TdElement>
                                            { sizeWindows === 'xl' && <TdElement> {elem.release} </TdElement> }
                                            <TdElement> {elem.manufacturer}  </TdElement>
                                            <TdElement style_c={'text-align: left;'}> {elem.model} </TdElement>
                                            <TdElement> {elem.hash} </TdElement>
                                            { sizeWindows === 'xl' && <TdElement style_c={'text-align: left;'}> {elem.algorithm} </TdElement> }
                                            <TdElement> {elem.efficiency} </TdElement>
                                            <TdElement style_c={'text-align: left;'}> <span className={style.green_c} >${elem.profit.price}</span>  / {elem.profit.term} </TdElement>
                                            <TdElement> <div className={style.white_c}> {`$${elem.price.min} - $${elem.price.max}`} </div></TdElement>
                                        </TrElement>
                                    )
                                })
                            }

                        </WrapperTBody>
                    </WrapperTable>
                </WrapperText>
        }


        {
            sizeWindows !== 'xl' && sizeWindows !== 'lg' &&
            <WrapperList style_c={wrapper_list_Style}>
                {
                    products &&  products.map( elem => {
                        return(
                            <ItemList isCard={card.some( p => p.id === elem.id) && true } onclick={ () => onFn(elem) } key={elem.id}>
                                <WrapperText style_c={style_block_top}>
                                    <Col>
                                        <MainText style_c={style_text}>{elem.manufacturer} {elem.model}</MainText>
                                        <MainText style_c={style_text}>{elem.hash}</MainText>
                                    </Col>
                                    <Col style_c={'display: flex;align-items: center;'}>
                                        <Image src={starIcon} alt="star" />
                                    </Col>
                                </WrapperText>
                                <WrapperText style_c={'width: 100%;text-align: center;'}>
                                    {sizeWindows === 'md' && <Image style_c={style_img} src={ elem.img.large} alt={elem.model} /> }
                                    {sizeWindows === 'sm' && <Image style_c={style_img} src={ elem.img.small} alt={elem.model} /> }
                                </WrapperText>
                                <WrapperText>
                                    <MainTitle style_c={style_price}>{`$${elem.price.min} - ${elem.price.max}`}</MainTitle>
                                </WrapperText>
                            </ItemList>
                        )
                    })
                }

            </WrapperList>
        }
        </>

    )
}


export default ProductsElement