import React, {useState, useEffect} from 'react';
import MainWrappers from "./Components/common/StylesElements/Wrappers/MainWrappers";
import Header from "./Components/Header/Header";
import Footer from "./Components/Footer/Footer";
import Home from "./Components/Home/Home";
import {compose} from "redux";
import { Route, withRouter, Switch} from 'react-router-dom';
import {connect} from "react-redux";
import PreloaderDefault from "./Components/common/Parts/Preloaders/PreloaderDefault";
import {initializeApp, checkWindowWidth} from "./Redux/app-reducer"

const App = (props) => {

    const [initializeMod, setInitializeMod] = useState(false)

    useEffect(() => {
        props.initializeApp()
        setInitializeMod(props.initialized)
        props.checkWindowWidth()
        windowsResize()
    }, [props.initialized]);



    const windowsResize = () => {
        window.addEventListener('resize', props.checkWindowWidth);
    }

    if(!initializeMod) {return <PreloaderDefault />}
  return (
      <MainWrappers style_c={'background: #0B0C23;'}>
          <Header card={props.card} />
          <Switch>
              <Route exact path='/' render={() => <Home/>}/>
              <Route path="*" render={() => <div>404</div>} />
          </Switch>
          <Footer/>
      </MainWrappers>
  );
}


const mapStateToProps = (state) => {
    return{
        initialized: state.app.initialized,

    }
}

export default compose(
    withRouter,
    connect( mapStateToProps, {initializeApp, checkWindowWidth} )
)(App)
