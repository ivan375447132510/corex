import {pageAPI} from "../api/api";

const INITIALAIZED_SUCSSES = 'redux/app/INITIALAIZED_SUCSSES'
const  SET_PRODUCTS = 'redux/app/SET_PRODUCTS'
const STATE_WINDOW_WIDTH = 'redux/app/STATE_WINDOW_WIDTH'
const SET_CARD = 'redux/app/SET_CARD'
const TOGGLE_IS_PRODUCT_PROGRESS = 'redux/app/TOGGLE_IS_PRODUCT_PROGRESS'
const LAST_ADDED_ITEM = 'redux/app/LAST_ADDED_ITEM'

let initialState = {
    initialized: false,
    product: [],
    card: [],
    sizeWindows: 'xl',
    productInProgress: [],
    lastItem: null
}



const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIALAIZED_SUCSSES:
            return{
                ...state, initialized: true
            }
        case SET_PRODUCTS:
            return {
                ...state, product: [...action.products]
            }
        case STATE_WINDOW_WIDTH:
            return {
                ...state, sizeWindows: action.stateWindowWidth
            }
        case SET_CARD:
            return {
                ...state, card: [...state.card, action.card]
            }
            case TOGGLE_IS_PRODUCT_PROGRESS:
            return {
                ...state,
                productInProgress: action.isProduct
                    ? [...state.productInProgress, action.productId ]
                    : state.productInProgress.filter(id => id !== action.productId)
            }
            case LAST_ADDED_ITEM:
            return {
                ...state, lastItem: action.productData

            }
        default:
            return state
    }
}


export const initializedSucsses = () => ({type: INITIALAIZED_SUCSSES})

export const initializeApp = () => async (dispatch) => {

    await setTimeout( () => {
        dispatch(initializedSucsses())
    }, 1500)

}

export const isWinwodWidth = (stateWindowWidth) => ({type: STATE_WINDOW_WIDTH, stateWindowWidth})

export const checkWindowWidth = () => (dispatch) => {
    if(window.innerWidth > 1439)dispatch(isWinwodWidth('xl'))
    if(window.innerWidth <= 1439 && window.innerWidth > 768)dispatch(isWinwodWidth('lg'))
    if(window.innerWidth <= 768 && window.innerWidth > 425)dispatch(isWinwodWidth('md'))
    if(window.innerWidth <= 425)dispatch(isWinwodWidth('sm'))
}


export const setProducts = (products) => ({type: SET_PRODUCTS, products})

export const getProducts = () => async (dispatch) => {
        const products = await pageAPI.getProduct()
        products.statusCod === '0' && dispatch(setProducts(products.data))
}


export const toggleProductProgress = (isProduct, productId) => ({type: TOGGLE_IS_PRODUCT_PROGRESS, isProduct, productId});
export const lastAddedItem = (productData) => ({type: LAST_ADDED_ITEM, productData});

export const setCard = (card) => ({type: SET_CARD, card})

export const cardAdd = (elem) => async (dispatch) => {
    dispatch(toggleProductProgress(true, elem.id))
    await setTimeout( () => {
        dispatch(toggleProductProgress(false, elem.id))
        dispatch(setCard(elem))
        dispatch(lastAddedItem({id: elem.id, model: elem.model}))
    }, 1000)
}






export default appReducer;