import * as axios from 'axios';

const instance = axios.create({
    baseURL: 'https://corex.bkborisov.by/',
    withCredentials: true
});
axios.defaults.headers.common['Accept'] = 'application/json'

export const pageAPI = {
    getProduct() {
        return instance.get().then(response => response.data)

    },

}







